<?php

namespace DiskoPete\LaravelMetrics\Tests\Php\Utils\Models;


use Illuminate\Database\Eloquent\Model;
use DiskoPete\LaravelMetrics\Traits\HasImages;

class Robot extends Model
{

    const TABLE_NAME = 'robot';

    protected $table = self::TABLE_NAME;
}
