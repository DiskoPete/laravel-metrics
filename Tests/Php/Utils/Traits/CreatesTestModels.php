<?php

namespace DiskoPete\LaravelMetrics\Tests\Php\Utils\Traits;


use DiskoPete\LaravelMetrics\Models\Record;
use DiskoPete\LaravelMetrics\Tests\Php\Utils\Models\Robot;
use Illuminate\Database\Eloquent\Model;

trait CreatesTestModels
{
    private function createRobot(): Robot
    {
        return factory(Robot::class)->create();
    }

    private function createRecord(array $attributes = []): Record
    {
        return factory(Record::class)->create($attributes);
    }

    private function createRecordForSubject(Model $subject): Record
    {
        return factory(Record::class)->create([
            Record::COLUMN_SUBJECT_TYPE => get_class($subject),
            Record::COLUMN_SUBJECT_ID   => $subject->getKey()
        ]);
    }
}
