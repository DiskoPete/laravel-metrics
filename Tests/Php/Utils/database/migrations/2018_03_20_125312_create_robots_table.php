<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use DiskoPete\LaravelMetrics\Tests\Php\Utils\Models\Robot;

class CreateRobotsTable extends Migration
{
    public function up()
    {
        Schema::create(Robot::TABLE_NAME, function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists(Robot::TABLE_NAME);
    }
}
