<?php
/**
 * @var Factory $factory
 */

use DiskoPete\LaravelMetrics\Models\Record;
use DiskoPete\LaravelMetrics\Tests\Php\Utils\Models\Robot;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;


$factory->define(Record::class, function (Faker $faker) {

    return [
        Record::COLUMN_ACTION => $faker->word,
        Record::COLUMN_VALUE  => 1
    ];
});

$factory->afterMaking(Record::class, function (Record $record) {

    if ($record->subject_type) {
        return;
    }

    $robot = factory(Robot::class)->create();

    $record->subject_type = get_class($robot);
    $record->subject_id   = $robot->getKey();
});
