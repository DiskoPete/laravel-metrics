<?php

namespace DiskoPete\LaravelMetrics\Tests\Php;


use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Storage;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->withoutExceptionHandling();
        $this->withFactories(__DIR__ . '/Utils/database/factories');
        $this->loadMigrationsFrom(__DIR__ . '/Utils/database/migrations');
    }

    protected function getPackageProviders($app)
    {
        return $this->readComposerJson()['extra']['laravel']['providers'];
    }

    private function readComposerJson(): array
    {
        $content = file_get_contents(__DIR__ . '/../../composer.json');

        return json_decode($content, true);
    }

    protected function getPathFromUrl(string $url): string
    {
        $path = parse_url($url, PHP_URL_PATH);
        $path = str_replace('/storage/', '', $path);

        $filesystem = $this->getFilesystem();

        return $filesystem->path($path);
    }

    private function getFilesystem(): FilesystemAdapter
    {
        return Storage::disk('public');
    }
}