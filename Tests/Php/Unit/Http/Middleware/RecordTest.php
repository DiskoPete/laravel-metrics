<?php


namespace DiskoPete\LaravelMetrics\Tests\Php\Unit\Http\Middleware;


use DiskoPete\LaravelMetrics\Contracts\Record\Register;
use DiskoPete\LaravelMetrics\Events\Middleware\BeforeRecord;
use DiskoPete\LaravelMetrics\Http\Middleware\Record;
use DiskoPete\LaravelMetrics\Tests\Php\TestCase;
use DiskoPete\LaravelMetrics\Tests\Php\Utils\Models\Robot;
use DiskoPete\LaravelMetrics\Tests\Php\Utils\Traits\CreatesTestModels;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Route;
use Jaybizzle\CrawlerDetect\CrawlerDetect;
use Mockery\MockInterface;

class RecordTest extends TestCase
{
    use RefreshDatabase;
    use CreatesTestModels;

    /**
     * @test
     */
    public function recordsViewForBoundModels(): void
    {
        $robot = $this->createRobot();

        $this->mock(Register::class, function (MockInterface $register) use ($robot) {
            $register
                ->shouldReceive('recordOnceForSession')
                ->withArgs(function (Model $model, string $action, int $value) use ($robot) {

                    self::assertTrue($model->is($robot));
                    self::assertEquals('robot_view', $action);
                    self::assertEquals(1, $value);

                    return true;
                })
                ->once()
                ->andReturnSelf();
        });

        $this
            ->get('robots/' . $robot->getRouteKey())
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function emitsEventWhichPreventsRecord(): void
    {
        $robot = $this->createRobot();

        $this->mock(Register::class, function (MockInterface $register) {
            $register->shouldNotReceive('recordOnceForSession');
        });

        Event::listen(
            BeforeRecord::class,
            function (BeforeRecord $event) {
                $event->shouldRecord = false;
            }
        );

        $this
            ->get('robots/' . $robot->getRouteKey())
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function doesNotRecordCrawlers(): void
    {
        $robot = $this->createRobot();

        $this->mock(Register::class, function (MockInterface $register) {
            $register->shouldNotReceive('recordOnceForSession');
        });


        $this->mock(CrawlerDetect::class, function (MockInterface $crawlerDetect) {
            $crawlerDetect->shouldReceive('isCrawler')->once()->andReturn(true);
        });

        $this
            ->get('robots/' . $robot->getRouteKey())
            ->assertStatus(200);
    }

    protected function setUp(): void
    {
        parent::setUp();

        Route::get('robots/{robot}', function (Robot $robot) {
            return $robot;
        })
            ->middleware('web')
            ->middleware(Record::class);
    }
}
