<?php


namespace DiskoPete\LaravelMetrics\Tests\Php\Unit\Models\Adapter\Db;


use Carbon\Carbon;
use DiskoPete\LaravelMetrics\Models\Adapter\Db\Collection;
use DiskoPete\LaravelMetrics\Models\Record;
use DiskoPete\LaravelMetrics\Tests\Php\TestCase;
use DiskoPete\LaravelMetrics\Tests\Php\Utils\Traits\CreatesTestModels;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CollectionTest extends TestCase
{
    use RefreshDatabase;
    use CreatesTestModels;

    /**
     * @test
     */
    public function canAggregateValue(): void
    {
        $this->createRecord([
            Record::COLUMN_VALUE => 1
        ]);

        $this->createRecord([
            Record::COLUMN_VALUE => 2
        ]);

        $this->createRecord([
            Record::COLUMN_VALUE => 1
        ]);

        $collection = $this->getCollection();

        self::assertEquals(4, $collection->aggregateValue());
    }

    private function getCollection(): Collection
    {
        return Record::all();
    }

    /**
     * @test
     */
    public function canGroupValuesByDay(): void
    {
        $this->createRecord([
            Record::COLUMN_VALUE      => 1,
            Record::COLUMN_CREATED_AT => Carbon::now()->subDay()
        ]);

        $this->createRecord([
            Record::COLUMN_VALUE => 2
        ]);

        $this->createRecord([
            Record::COLUMN_VALUE      => 3,
            Record::COLUMN_CREATED_AT => Carbon::now()->addDay()

        ]);

        $groupedValue = $this->getCollection()->groupValue();

        self::assertEquals(1, $groupedValue->shift());
        self::assertEquals(2, $groupedValue->shift());
        self::assertEquals(3, $groupedValue->shift());

    }
}