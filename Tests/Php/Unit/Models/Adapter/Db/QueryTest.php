<?php


namespace DiskoPete\LaravelMetrics\Tests\Php\Unit\Models\Adapter\Db;


use Carbon\Carbon;
use DiskoPete\LaravelMetrics\Models\Adapter\Db\Query;
use DiskoPete\LaravelMetrics\Models\Adapter\Db\Query\Factory;
use DiskoPete\LaravelMetrics\Models\Record;
use DiskoPete\LaravelMetrics\Tests\Php\TestCase;
use DiskoPete\LaravelMetrics\Tests\Php\Utils\Models\Robot;
use DiskoPete\LaravelMetrics\Tests\Php\Utils\Traits\CreatesTestModels;
use Illuminate\Foundation\Testing\RefreshDatabase;

class QueryTest extends TestCase
{
    use RefreshDatabase;
    use CreatesTestModels;

    /**
     * @test
     */
    public function canFilterBySubject(): void
    {
        $c3po = $this->createRobot();
        $r2d2 = $this->createRobot();

        $recordC3po = $this->createRecordForSubject($c3po);
        $recordR2d2 = $this->createRecordForSubject($r2d2);

        $collection = $this->makeQuery()
            ->addSubjectFilter($c3po)
            ->get();

        self::assertTrue($collection->contains($recordC3po));
        self::assertFalse($collection->contains($recordR2d2));
    }

    /**
     * @test
     */
    public function canFilterBySubjects(): void
    {
        $c3po = $this->createRobot();
        $r2d2 = $this->createRobot();

        $recordC3po = $this->createRecordForSubject($c3po);
        $recordR2d2 = $this->createRecordForSubject($r2d2);

        $collection = $this->makeQuery()
            ->addSubjectsFilter(Robot::query()->whereKey($c3po->getKey())->get())
            ->get();

        self::assertTrue($collection->contains($recordC3po));
        self::assertFalse($collection->contains($recordR2d2));
    }

    private function makeQuery(): Query
    {
        return $this->getFactory()->make();
    }

    private function getFactory(): Factory
    {
        return app(Factory::class);
    }

    /**
     * @test
     */
    public function canFilterByDateRange(): void
    {
        $pastRecord   = $this->createRecord([
            Record::COLUMN_CREATED_AT => Carbon::now()->subWeek()
        ]);
        $record       = $this->createRecord();
        $futureRecord = $this->createRecord([
            Record::COLUMN_CREATED_AT => Carbon::now()->addWeek()
        ]);

        $collection = $this->makeQuery()
            ->addDateRangeFilter(
                Carbon::now()->subDay(),
                Carbon::now()->addDay()
            )
            ->get();

        self::assertTrue($collection->contains($record));
        self::assertFalse($collection->contains($pastRecord));
        self::assertFalse($collection->contains($futureRecord));
    }

    /**
     * @test
     */
    public function canFilterByAction(): void
    {
        $fooRecord   = $this->createRecord([
            Record::COLUMN_ACTION => 'foo'
        ]);
        $barRecord = $this->createRecord([
            Record::COLUMN_ACTION => 'bar'
        ]);

        $collection = $this->makeQuery()
            ->addActionFilter('foo')
            ->get();

        self::assertTrue($collection->contains($fooRecord));
        self::assertFalse($collection->contains($barRecord));
    }
}