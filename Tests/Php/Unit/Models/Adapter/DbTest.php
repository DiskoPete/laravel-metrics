<?php


namespace DiskoPete\LaravelMetrics\Tests\Php\Unit\Models\Adapter;


use DiskoPete\LaravelMetrics\Contracts\Record\Data;
use DiskoPete\LaravelMetrics\Models\Adapter\Db;
use DiskoPete\LaravelMetrics\Models\Record;
use DiskoPete\LaravelMetrics\Tests\Php\TestCase;
use DiskoPete\LaravelMetrics\Tests\Php\Utils\Traits\CreatesTestModels;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DbTest extends TestCase
{
    use RefreshDatabase;
    use CreatesTestModels;

    /**
     * @test
     */
    public function savesRecordToDatabase(): void
    {
        $robot  = $this->createRobot();
        $writer = $this->getWriter();
        $data   = $this->makeData();

        $data->setSubject($robot);
        $data->setAction('foobar');
        $data->setValue(1);

        $writer->write($data);

        $this->assertDatabaseHas(
            Record::TABLE_NAME,
            [
                Record::COLUMN_SUBJECT_TYPE => get_class($robot),
                Record::COLUMN_SUBJECT_ID   => $robot->getKey(),
                Record::COLUMN_ACTION       => 'foobar',
                Record::COLUMN_VALUE        => 1,
            ]
        );
    }

    private function makeData(): Data
    {
        return $this->app->make(Data::class);
    }

    private function getWriter(): Db
    {
        return $this->app->make(Db::class);
    }
}