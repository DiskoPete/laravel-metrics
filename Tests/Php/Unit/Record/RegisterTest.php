<?php


namespace DiskoPete\LaravelMetrics\Tests\Php\Unit\Record;


use DiskoPete\LaravelMetrics\Contracts\Record\Register;
use DiskoPete\LaravelMetrics\Jobs\Record;
use DiskoPete\LaravelMetrics\Tests\Php\TestCase;
use DiskoPete\LaravelMetrics\Tests\Php\Utils\Traits\CreatesTestModels;
use Illuminate\Support\Facades\Bus;

class RegisterTest extends TestCase
{
    use CreatesTestModels;

    /**
     * @var \Illuminate\Support\Testing\Fakes\BusFake
     */
    private $busFake;

    protected function setUp(): void
    {
        parent::setUp();

        $this->busFake = Bus::fake();
    }

    /**
     * @test
     */
    public function canDispatchRecordJob(): void
    {
        $robot = $this->createRobot();
        $register = $this->getRegister();
        $register->recordOnceForSession(
            $robot,
            'view',
            1
        );

        $this->busFake->assertDispatched(Record::class);
    }

    /**
     * @test
     */
    public function hasMethodToOnlyDispatchJobOncePerSession(): void
    {
        $robot = $this->createRobot();
        $register = $this->getRegister();
        $register->recordOnceForSession(
            $robot,
            'view',
            1
        );
        $register->recordOnceForSession(
            $robot,
            'view',
            1
        );

        $this->busFake->assertDispatched(Record::class, 1);
    }

    private function getRegister(): Register
    {
        return $this->app->make(Register::class);
    }
}