<?php


namespace DiskoPete\LaravelMetrics\Tests\Php\Unit\Jobs;


use DiskoPete\LaravelMetrics\Contracts\Record\Data;
use DiskoPete\LaravelMetrics\Jobs\Record;
use DiskoPete\LaravelMetrics\Models\Adapter\Db;
use DiskoPete\LaravelMetrics\Tests\Php\TestCase;
use DiskoPete\LaravelMetrics\Tests\Php\Utils\Traits\CreatesTestModels;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery\MockInterface;

class RecordTest extends TestCase
{
    use CreatesTestModels;
    use RefreshDatabase;

    /**
     * @test
     */
    public function passesDataToAllAdapters(): void
    {
        $robot = $this->createRobot();
        $job   = new Record(
            $robot,
            'foobar',
            1
        );

        $this->mock(Db::class, function (MockInterface $db) use ($robot) {

            $db
                ->shouldReceive('write')
                ->withArgs(function(Data $data) use ($robot){

                    self::assertTrue($data->getSubject()->is($robot));
                    self::assertEquals('foobar', $data->getAction());
                    self::assertEquals(1, $data->getValue());

                    return true;
                })
                ->once()
                ->andReturnSelf();

        });

        $job->handle();
    }
}
