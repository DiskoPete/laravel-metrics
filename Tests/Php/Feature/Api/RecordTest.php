<?php


namespace DiskoPete\LaravelMetrics\Tests\Php\Feature\Api;


use DiskoPete\LaravelMetrics\Contracts\Record\Register;
use DiskoPete\LaravelMetrics\Contracts\TypeMapper;
use DiskoPete\LaravelMetrics\Http\Controllers\Api\Record;
use DiskoPete\LaravelMetrics\Tests\Php\TestCase;
use DiskoPete\LaravelMetrics\Tests\Php\Utils\Models\Robot;
use DiskoPete\LaravelMetrics\Tests\Php\Utils\Traits\CreatesTestModels;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery\MockInterface;

class RecordTest extends TestCase
{
    use RefreshDatabase;
    use CreatesTestModels;

    /**
     * @test
     */
    public function canRecordWithApiCall(): void
    {
        $robot = $this->createRobot();

        $this->mock(Register::class, function (MockInterface $register) use ($robot) {

            $register
                ->shouldReceive('record')
                ->once()
                ->withArgs(function (Model $subject, string $action, int $value) use ($robot) {

                    self::assertTrue($subject->is($robot));
                    self::assertEquals('foobar', $action);
                    self::assertEquals(3, $value);

                    return true;
                });

        });

        $this
            ->postJson('api/metrics/record', [
                Record::KEY_SUBJECT_TYPE => 'robot',
                Record::KEY_SUBJECT_ID   => $robot->getKey(),
                Record::KEY_ACTION       => 'foobar',
                Record::KEY_VALUE        => 3
            ])
            ->assertStatus(200);


    }

    protected function setUp(): void
    {
        parent::setUp();


        $this->getTypeMapper()->register('robot', Robot::class);

    }

    private function getTypeMapper(): TypeMapper
    {
        return $this->app->make(TypeMapper::class);
    }
}
