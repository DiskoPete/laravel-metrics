<?php


namespace DiskoPete\LaravelMetrics\Contracts\Record;


interface Writer
{
    public function write(Data $data): Writer;
}
