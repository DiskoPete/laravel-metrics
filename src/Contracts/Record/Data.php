<?php


namespace DiskoPete\LaravelMetrics\Contracts\Record;


use Illuminate\Database\Eloquent\Model;

interface Data
{
    public function setSubject(Model $subject): Data;

    public function getSubject(): Model;

    public function setAction(string $action): Data;

    public function getAction(): string;

    public function setValue(int $value): Data;

    public function getValue(): int;
}
