<?php


namespace DiskoPete\LaravelMetrics\Contracts\Record;


use Illuminate\Database\Eloquent\Model;

interface Register
{
    public function recordOnceForSession(
        Model $subject,
        string $action,
        int $value = 1
    ): Register;

    public function record(
        Model $subject,
        string $action,
        int $value = 1
    ): Register;
}
