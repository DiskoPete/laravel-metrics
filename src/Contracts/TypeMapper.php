<?php


namespace DiskoPete\LaravelMetrics\Contracts;


interface TypeMapper
{
    public function register(string $key, string $type): TypeMapper;

    public function map(string $key): string;
}
