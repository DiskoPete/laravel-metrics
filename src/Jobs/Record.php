<?php

namespace DiskoPete\LaravelMetrics\Jobs;

use DiskoPete\LaravelMetrics\Contracts\Record\Data;
use DiskoPete\LaravelMetrics\Contracts\Record\Writer;
use DiskoPete\LaravelMetrics\Models\Adapter\Db;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class Record implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * @var Model
     */
    private $subject;
    /**
     * @var string
     */
    private $action;
    /**
     * @var int
     */
    private $value;

    public function __construct(
        Model $subject,
        string $action,
        int $value
    )
    {
        $this->subject = $subject;
        $this->action  = $action;
        $this->value   = $value;
    }

    public function handle()
    {

        $data = $this->prepareData();

        $this->getAdapter()
            ->each(function (Writer $adapter) use ($data) {
                $adapter->write($data);
            });
    }

    private function getAdapter(): Collection
    {
        return collect([
            app(Db::class),
        ]);
    }

    private function prepareData(): Data
    {
        $data = $this->makeData();
        $data->setSubject($this->subject);
        $data->setAction($this->action);
        $data->setValue($this->value);

        return $data;
    }

    private function makeData(): Data
    {
        return app(Data::class);
    }
}
