<?php


namespace DiskoPete\LaravelMetrics\Http\Middleware;


use Closure;
use DiskoPete\LaravelMetrics\Contracts\Record\Register;
use DiskoPete\LaravelMetrics\Events\Middleware\BeforeRecord;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Str;
use Jaybizzle\CrawlerDetect\CrawlerDetect;

class Record
{
    /**
     * @var Register
     */
    private $register;
    /**
     * @var Dispatcher
     */
    private $eventDispatcher;
    /**
     * @var CrawlerDetect
     */
    private $crawlerDetect;

    public function __construct(
        Register $register,
        Dispatcher $eventDispatcher,
        CrawlerDetect $crawlerDetect
    )
    {
        $this->register        = $register;
        $this->eventDispatcher = $eventDispatcher;
        $this->crawlerDetect = $crawlerDetect;
    }

    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        if (!$this->crawlerDetect->isCrawler()) {
            $this->recordBoundModels($request->route());
        }

        return $response;
    }

    private function recordBoundModels(Route $route): void
    {
        foreach ($route->parameters() as $parameter) {

            if (!$this->shouldProcess($parameter)) {
                continue;
            }

            $this->record($parameter);
        }
    }

    private function shouldProcess($parameter): bool
    {
        if (!$parameter instanceof Model) {
            return false;
        }

        $event = new BeforeRecord($parameter);

        $this->eventDispatcher->dispatch($event);

        return $event->shouldRecord;
    }

    private function record(Model $subject): void
    {
        $name = Str::snake(class_basename($subject));

        $this->register->recordOnceForSession(
            $subject,
            "{$name}_view",
            1);
    }
}
