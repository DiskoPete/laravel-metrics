<?php


namespace DiskoPete\LaravelMetrics\Http\Controllers\Api;


use DiskoPete\LaravelMetrics\Contracts\Record\Register;
use DiskoPete\LaravelMetrics\Contracts\TypeMapper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class Record extends Controller
{
    const KEY_SUBJECT_TYPE = 'subject_type';
    const KEY_SUBJECT_ID   = 'subject_id';
    const KEY_ACTION       = 'action';
    const KEY_VALUE        = 'value';
    /**
     * @var Register
     */
    private $register;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var TypeMapper
     */
    private $typeMapper;

    public function __construct(
        Register $register,
        Request $request,
        TypeMapper $typeMapper
    )
    {
        $this->register   = $register;
        $this->request    = $request;
        $this->typeMapper = $typeMapper;
    }

    public function execute(): void
    {
        $this->request->validate([
            self::KEY_SUBJECT_TYPE => 'required',
            self::KEY_SUBJECT_ID   => 'required',
            self::KEY_ACTION       => 'required',
        ]);

        $subject = $this->findSubject();

        $this->register->record(
            $subject,
            $this->request->input(self::KEY_ACTION),
            $this->request->input(self::KEY_VALUE, 1)
        );
    }

    private function findSubject(): Model
    {
        $type = $this->typeMapper->map($this->request->input(self::KEY_SUBJECT_TYPE));

        return $type::query()->findOrFail(
            $this->request->input(self::KEY_SUBJECT_ID)
        );
    }
}
