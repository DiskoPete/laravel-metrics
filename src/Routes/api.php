<?php

use DiskoPete\LaravelMetrics\Http\Controllers\Api\Record;
use Illuminate\Support\Facades\Route;

Route::post('metrics/record', [Record::class, 'execute']);
