<?php


namespace DiskoPete\LaravelMetrics\Events\Middleware;


use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BeforeRecord
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $shouldRecord = true;
    /**
     * @var Model
     */
    private $subject;

    public function __construct(Model $subject)
    {
        $this->subject = $subject;
    }

    public function getSubject(): Model
    {
        return $this->subject;
    }
}
