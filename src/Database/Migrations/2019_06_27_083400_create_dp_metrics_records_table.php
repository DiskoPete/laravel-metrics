<?php

use DiskoPete\LaravelMetrics\Models\Record;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDpMetricsRecordsTable extends Migration
{
    public function up()
    {
        Schema::create(Record::TABLE_NAME, function (Blueprint $table) {

            $table->increments(Record::COLUMN_ID);
            $table->string(Record::COLUMN_SUBJECT_TYPE);
            $table->integer(Record::COLUMN_SUBJECT_ID);
            $table->string(Record::COLUMN_ACTION);
            $table->integer(Record::COLUMN_VALUE);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists(Record::TABLE_NAME);
    }
}
