<?php

namespace DiskoPete\LaravelMetrics\Providers;

use DiskoPete\LaravelMetrics\Contracts\Record\Data as DataContract;
use DiskoPete\LaravelMetrics\Contracts\Record\Register as RegisterContract;
use DiskoPete\LaravelMetrics\Contracts\TypeMapper as TypeMapperContract;
use DiskoPete\LaravelMetrics\Models\Adapter\Db;
use DiskoPete\LaravelMetrics\Models\Adapter\Db\Query\Factory as DbQueryFactory;
use DiskoPete\LaravelMetrics\Models\Record\Data;
use DiskoPete\LaravelMetrics\Models\Record\Register;
use DiskoPete\LaravelMetrics\Models\TypeMapper;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{

    public function boot()
    {
        $this->loadMigrations();
    }

    private function loadMigrations()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    public function register()
    {
        parent::register();

        $this->app->register(RouteServiceProvider::class);

        $this->app->singleton(
            DataContract::class,
            Data::class
        );

        $this->app->singleton(Db::class);
        $this->app->singleton(DbQueryFactory::class);

        $this->app->singleton(
            RegisterContract::class,
            Register::class
        );

        $this->app->singleton(
            TypeMapperContract::class,
            TypeMapper::class
        );
    }
}
