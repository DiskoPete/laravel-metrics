<?php


namespace DiskoPete\LaravelMetrics\Models\Adapter;


use DiskoPete\LaravelMetrics\Contracts\Record\Data;
use DiskoPete\LaravelMetrics\Contracts\Record\Writer;
use DiskoPete\LaravelMetrics\Models\Record;

class Db implements Writer
{

    public function write(Data $data): Writer
    {
        $record  = new Record();
        $subject = $data->getSubject();

        $record->setAttribute(Record::COLUMN_SUBJECT_TYPE, get_class($subject));
        $record->setAttribute(Record::COLUMN_SUBJECT_ID, $subject->getKey());
        $record->setAttribute(Record::COLUMN_ACTION, $data->getAction());
        $record->setAttribute(Record::COLUMN_VALUE, $data->getValue());

        $record->save();

        return $this;
    }
}
