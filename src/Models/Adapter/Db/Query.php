<?php


namespace DiskoPete\LaravelMetrics\Models\Adapter\Db;


use DiskoPete\LaravelMetrics\Models\Record;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Model;

/**
 * @method Collection get($columns = ['*'])
 */
class Query extends Builder
{

    public function addSubjectFilter(Model $subject): Query
    {
        return $this->where([
            Record::COLUMN_SUBJECT_TYPE => get_class($subject),
            Record::COLUMN_SUBJECT_ID   => $subject->getKey()
        ]);
    }

    public function addDateRangeFilter($from, $to): Query
    {
        return $this->whereBetween(
            Record::COLUMN_CREATED_AT,
            [
                $from,
                $to
            ]
        );
    }

    public function addActionFilter(string $action): Query
    {
        return $this->where(Record::COLUMN_ACTION, $action);
    }

    public function addSubjectsFilter(EloquentCollection $subjects): Query
    {
        return $this
            ->where(
                Record::COLUMN_SUBJECT_TYPE,
                get_class($subjects->first())
            )
            ->whereIn(
                Record::COLUMN_SUBJECT_ID,
                $subjects->modelKeys()
            );
    }
}
