<?php


namespace DiskoPete\LaravelMetrics\Models\Adapter\Db;


use DiskoPete\LaravelMetrics\Models\Record;

class Collection extends \Illuminate\Database\Eloquent\Collection
{

    public function groupValue(): \Illuminate\Support\Collection
    {
        return $this
            ->groupBy(function (Record $record) {
                return $record->created_at->format('Y-m-d');
            })
            ->map(function (Collection $records) {
                return $records->aggregateValue();
            });
    }

    public function aggregateValue(): int
    {
        return $this->reduce(function (int $carry, Record $record) {
            return $carry + $record->value;
        }, 0);
    }
}
