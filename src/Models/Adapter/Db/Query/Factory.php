<?php


namespace DiskoPete\LaravelMetrics\Models\Adapter\Db\Query;


use DiskoPete\LaravelMetrics\Models\Adapter\Db\Query;
use DiskoPete\LaravelMetrics\Models\Record;

class Factory
{
    public function make(): Query
    {
        return (new Record())->newQueryWithoutScopes();
    }
}