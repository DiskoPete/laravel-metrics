<?php


namespace DiskoPete\LaravelMetrics\Models;


use DiskoPete\LaravelMetrics\Contracts\TypeMapper as TypeMapperContract;

class TypeMapper implements TypeMapperContract
{

    private $types = [];

    public function register(string $key, string $type): TypeMapperContract
    {
        if (array_key_exists($key, $this->types)) {
            throw new \RuntimeException("{$key} is already registered.");
        }

        $this->types[$key] = $type;

        return $this;
    }

    public function map(string $key): string
    {
        if (!array_key_exists($key, $this->types)) {
            throw new \RuntimeException("{$key} is not registered.");
        }

        return $this->types[$key];
    }
}
