<?php


namespace DiskoPete\LaravelMetrics\Models;


use Carbon\Carbon;
use DiskoPete\LaravelMetrics\Models\Adapter\Db\Collection;
use DiskoPete\LaravelMetrics\Models\Adapter\Db\Query;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string subject_type
 * @property  int subject_id
 * @property int value
 * @property Carbon created_at
 */
class Record extends Model
{
    public const COLUMN_ACTION       = 'action';
    public const COLUMN_CREATED_AT   = 'created_at';
    public const COLUMN_ID           = 'id';
    public const COLUMN_SUBJECT_TYPE = 'subject_type';
    public const COLUMN_SUBJECT_ID   = 'subject_id';
    public const COLUMN_VALUE        = 'value';

    public const TABLE_NAME = 'dp_metrics_records';

    protected $table = self::TABLE_NAME;

    protected $primaryKey = self::COLUMN_ID;

    protected $casts = [
        self::COLUMN_VALUE => 'int'
    ];

    public function newEloquentBuilder($query): Query
    {
        return app(Query::class, ['query' => $query]);
    }

    public function newCollection(array $models = [])
    {
        return new Collection($models);
    }
}
