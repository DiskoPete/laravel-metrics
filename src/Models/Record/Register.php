<?php


namespace DiskoPete\LaravelMetrics\Models\Record;


use DiskoPete\LaravelMetrics\Contracts\Record\Register as RegisterContract;
use DiskoPete\LaravelMetrics\Jobs\Record;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Session\Store;

class Register implements RegisterContract
{
    const SESSION_KEY = 'metrics_recorded';

    /**
     * @var Dispatcher
     */
    private $dispatcher;
    /**
     * @var Store
     */
    private $session;

    public function __construct(
        Dispatcher $dispatcher,
        Store $session
    )
    {
        $this->dispatcher = $dispatcher;
        $this->session    = $session;
    }

    public function recordOnceForSession(Model $subject, string $action, int $value = 1): RegisterContract
    {
        $hash = $this->makeRecordHash($subject, $action);

        if ($this->alreadyRecorded($hash)) {
            return $this;
        }

        $this->record($subject, $action, $value);

        $this->markAsRecorded($hash);

        return $this;
    }

    private function makeRecordHash(Model $subject, string $action): string
    {
        $fragments = [
            get_class($subject),
            $subject->getKey(),
            $action
        ];

        return md5(implode('', $fragments));
    }

    private function alreadyRecorded(string $hash): bool
    {
        $sessionRecorded = $this->session->get(self::SESSION_KEY, []);

        return in_array($hash, $sessionRecorded);
    }

    public function record(Model $subject, string $action, int $value = 1): RegisterContract
    {
        $this->dispatcher->dispatch(
            new Record(
                $subject,
                $action,
                $value
            )
        );

        return $this;
    }

    private function markAsRecorded(string $hash): void
    {
        $this->session->push(self::SESSION_KEY, $hash);
    }
}
