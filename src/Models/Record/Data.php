<?php


namespace DiskoPete\LaravelMetrics\Models\Record;


use DiskoPete\LaravelMetrics\Contracts\Record\Data as DataContract;
use Illuminate\Database\Eloquent\Model;

class Data implements DataContract
{

    /**
     * @var Model
     */
    private $subject;
    /**
     * @var string
     */
    private $action;
    /**
     * @var int
     */
    private $value;

    public function getSubject(): Model
    {
        return $this->subject;
    }

    public function setSubject(Model $subject): DataContract
    {
        $this->subject = $subject;
        return $this;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function setAction(string $action): DataContract
    {
        $this->action = $action;
        return $this;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue(int $value): DataContract
    {
        $this->value = $value;
        return $this;
    }
}